#!/usr/bin/env bash

# Run as root or insert `sudo -E` before `bash`:

set -e

apt update -y;
apt -y upgrade;
apt -y install curl git;

if [ ! -d playa ]; then
	git clone https://playa_script:$PLAYA_SCRIPT_PASSWORD@gitlab.com/ott-platform/playa.git playa;
fi

function follow_links() (
  cd -P "$(dirname -- "$1")"
  file="$PWD/$(basename -- "$1")"
  while [[ -h "$file" ]]; do
    cd -P "$(dirname -- "$file")"
    file="$(readlink -- "$file")"
    cd -P "$(dirname -- "$file")"
    file="$PWD/$(basename -- "$file")"
  done
  echo "$file"
)

PROG_NAME="$(follow_links "${BASH_SOURCE[0]}")"

export PLAYA_SRC="${PROG_NAME%/*}/playa";

if  [[ ! -z ${PLAYA_SRC+x} && -d "$PLAYA_SRC" ]]; then
    cd $PLAYA_SRC;
	git checkout production;
	git pull;
else
	echo "Playa installation directory '$PLAYA_SRC' does not exist. Installation might have failed";
	exit 1;
fi

chmod +x ./setup.ubuntu.sh;
bash ./setup.ubuntu.sh;

function convertMovies() {
	echo 'Starting episodes conversion script';
	if "$NOT_USE_TMUX" = true ; then
		echo 'Running without TMUX'
		bash ./convert_movies.sh;
	else
		tmux new-session -d -s movies_session 'bash ./convert_movies.sh;';
		tmux ls;
		echo 'To attach, run "tmux attach-session -t movies_session"';
	fi
}

function convertEpisodes() {
	echo 'Starting episodes conversion script';
	if "$NOT_USE_TMUX" = true ; then
		echo 'Running without TMUX'
		bash ./convert_episodes.sh;
	else
		tmux new-session -d -s episodes_session 'bash ./convert_episodes.sh;';
		echo 'To attach, run "tmux attach-session -t episodes_session"';
	fi
}

function askForConversion() {
	read -p "Which type of videos should playa make downloadable URLs? Episodes or Movies? (E/M): " -n 1 -r
	echo

	if [[ $REPLY =~ ^[Mm]$ ]]; then
		convertMovies;
	elif [[ $REPLY =~ ^[Ee]$ ]]; then
		convertEpisodes;
	else
		echo 'No conversion made.';
	fi
}

command=$1

case "$command" in
E*|e*)
	convertEpisodes;
	;;
M*|m*)
	convertMovies;
	;;
*)
    askForConversion
    ;;
esac
